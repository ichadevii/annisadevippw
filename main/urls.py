from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.homepage1, name='profile'),
    path('profile2', views.homepage2, name='profile2'),
    path('hobby', views.hobby, name='hobby')
]
